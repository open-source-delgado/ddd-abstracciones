using Microsoft.EntityFrameworkCore;
using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;

namespace DDD.Abstracciones.NucleoCompartido.Repositorios.EFCore;

/// <inheritdoc/>
public class RepositorioDeConsultasEFCore<T> : IRepositorioDeConsultas<T> where T : class, IRaizAgregada
{
    private readonly DbContext _contexto;

    private readonly ISpecificationEvaluator _evaluadorDeEspecificaciones;

    /// <summary>
    /// Crea una nueva instancia de <see cref="RepositorioDeConsultasEFCore{T}"/>.
    /// </summary>
    /// <param name="contexto">Instancia de <see cref="DbContext"/></param>
    /// <param name="evaluadorDeEspecificaciones">Instancia de <see cref="ISpecificationEvaluator"/></param>
    protected RepositorioDeConsultasEFCore(DbContext contexto, ISpecificationEvaluator evaluadorDeEspecificaciones)
    {
        _contexto = contexto;
        _evaluadorDeEspecificaciones = evaluadorDeEspecificaciones;
    }

    /// <summary>
    /// Crea una nueva instancia de <see cref="RepositorioDeConsultasEFCore{T}"/>.
    /// </summary>
    /// <param name="contexto">Instancia de <see cref="DbContext"/></param>    
    public RepositorioDeConsultasEFCore(DbContext contexto) : this(contexto, SpecificationEvaluator.Default)
    {
    }

    /// <inheritdoc/>
    public virtual async Task<T?> BuscarPorId<TId>(TId id, CancellationToken tokenDeCancelacion = default) where TId : notnull
    {
        return await _contexto.Set<T>().FindAsync(new object[] { id }, cancellationToken: tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual IAsyncEnumerable<T> ComoEnumerable(ISpecification<T> especificacion)
    {
        return AplicarEspecificacion(especificacion).AsAsyncEnumerable();
    }

    /// <inheritdoc/>
    public virtual async Task<int> Contar(ISpecification<T> especificacion, CancellationToken tokenDeCancelacion = default)
    {
        return await AplicarEspecificacion(especificacion, true).CountAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task<int> Contar(CancellationToken tokenDeCancelacion = default)
    {
        return await _contexto.Set<T>().CountAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task<bool> ContieneElemento(ISpecification<T> especificacion, CancellationToken tokenDeCancelacion = default)
    {
        return await AplicarEspecificacion(especificacion, true).AnyAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task<bool> ContieneElemento(CancellationToken tokenDeCancelacion = default)
    {
        return await _contexto.Set<T>().AnyAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task<List<T>> Listar(CancellationToken tokenDeCancelacion = default)
    {
        return await _contexto.Set<T>().ToListAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task<List<T>> Listar(ISpecification<T> especificacion, CancellationToken tokenDeCancelacion = default)
    {
        var resultado = await AplicarEspecificacion(especificacion).ToListAsync(tokenDeCancelacion);

        return especificacion.PostProcessingAction == null ? resultado : especificacion.PostProcessingAction(resultado).ToList();
    }

    /// <inheritdoc/>
    public virtual async Task<List<TResultado>> Listar<TResultado>(ISpecification<T, TResultado> especificacion, CancellationToken tokenDeCancelacion = default)
    {
        var resultado = await AplicarEspecificacion(especificacion).ToListAsync(tokenDeCancelacion);

        return especificacion.PostProcessingAction == null ? resultado : especificacion.PostProcessingAction(resultado).ToList();
    }

    /// <inheritdoc/>
    public virtual async Task<T?> PrimeroOPredeterminado(ISpecification<T> especificacion, CancellationToken tokenDeCancelacion = default)
    {
        return await AplicarEspecificacion(especificacion).FirstOrDefaultAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task<TResultado?> PrimeroOPredeterminado<TResultado>(ISpecification<T, TResultado> especificacion, CancellationToken tokenDeCancelacion = default)
    {
        return await AplicarEspecificacion(especificacion).FirstOrDefaultAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task<T?> UnicoOPredeterminado(ISingleResultSpecification<T> especificacion, CancellationToken tokenDeCancelacion = default)
    {
        return await AplicarEspecificacion(especificacion).SingleOrDefaultAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task<TResultado?> UnicoOPredeterminado<TResultado>(ISingleResultSpecification<T, TResultado> especificacion, CancellationToken tokenDeCancelacion = default)
    {
        return await AplicarEspecificacion(especificacion).SingleOrDefaultAsync(tokenDeCancelacion);
    }

    /// <summary>
    /// Filtra las entidades de tipo <typeparamref name="T"/>, a aquellos que coinciden con la lógica de consulta encapsulada del
    /// <paramref name="especificacion"/>.
    /// </summary>
    /// <param name="especificacion">La lógica de consulta encapsulada.</param>
    /// <param name="evaluateCriteriaOnly">Si se debe evaluar solo los criterios de la especificación.</param>
    /// <returns>Las entidades filtradas como <see cref="IQueryable{T}"/>.</returns>
    protected virtual IQueryable<T> AplicarEspecificacion(ISpecification<T> especificacion, bool evaluateCriteriaOnly = false)
    {
        return _evaluadorDeEspecificaciones.GetQuery(_contexto.Set<T>().AsQueryable(), especificacion, evaluateCriteriaOnly);
    }

    /// <summary>
    /// Filtra las entidades de tipo <typeparamref name="T"/>, a aquellos que coinciden con la lógica de consulta encapsulada del
    /// <paramref name="especificacion"/>, de la base de datos.
    /// <para>
    /// Proyecta cada entidad en una nueva forma, siendo <typeparamref name="TResult" />.
    /// </para>
    /// </summary>
    /// <typeparam name="TResult">El tipo de valor devuelto por la proyección.</typeparam>
    /// <param name="especificacion">La lógica de consulta encapsulada.</param>
    /// <returns>Las entidades proyectadas filtradas como <see cref="IQueryable{T}"/>.</returns>
    protected virtual IQueryable<TResult> AplicarEspecificacion<TResult>(ISpecification<T, TResult> especificacion)
    {
        return _evaluadorDeEspecificaciones.GetQuery(_contexto.Set<T>().AsQueryable(), especificacion);
    }
}