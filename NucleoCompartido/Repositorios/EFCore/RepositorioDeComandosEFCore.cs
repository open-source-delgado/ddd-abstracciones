using Microsoft.EntityFrameworkCore;
using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using Ddd.Abstracciones.NucleoCompartido.Repositorios;

namespace DDD.Abstracciones.NucleoCompartido.Repositorios.EFCore;

/// <inheritdoc/>
public class RepositorioDeComandosEFCore<T> : IRepositorioDeComandos<T> where T : class, IRaizAgregada
{
    private readonly DbContext _contexto;

    private readonly ISpecificationEvaluator _evaluadorDeEspecificaciones;

    /// <summary>
    /// Crea una nueva instancia de <see cref="RepositorioDeComandosEFCore{T}"/>.
    /// </summary>
    /// <param name="contexto">Instancia de <see cref="DbContext"/></param>
    /// <param name="evaluadorDeEspecificaciones">Instancia de <see cref="ISpecificationEvaluator"/></param>
    protected RepositorioDeComandosEFCore(DbContext contexto, ISpecificationEvaluator evaluadorDeEspecificaciones)
    {
        _contexto = contexto;
        _evaluadorDeEspecificaciones = evaluadorDeEspecificaciones;
    }

    /// <summary>
    /// Crea una nueva instancia de <see cref="RepositorioDeComandosEFCore{T}"/>.
    /// </summary>
    /// <param name="contexto">Instancia de <see cref="DbContext"/></param>    
    public RepositorioDeComandosEFCore(DbContext contexto) : this(contexto, SpecificationEvaluator.Default)
    {
    }

    /// <inheritdoc/>
    public virtual async Task<T> Agregar(T entidad, CancellationToken tokenDeCancelacion = default)
    {
        await _contexto.Set<T>().AddAsync(entidad, tokenDeCancelacion);
        await _contexto.SaveChangesAsync(tokenDeCancelacion);
        return entidad;
    }

    /// <inheritdoc/>
    public virtual async Task<IEnumerable<T>> AgregarLista(IEnumerable<T> entidades, CancellationToken tokenDeCancelacion = default)
    {
        await _contexto.Set<T>().AddRangeAsync(entidades, tokenDeCancelacion);
        await _contexto.SaveChangesAsync(tokenDeCancelacion);
        return entidades;
    }

    /// <inheritdoc/>
    public virtual async Task Actualizar(T entidad, CancellationToken tokenDeCancelacion = default)
    {
        _contexto.Set<T>().Update(entidad);
        await _contexto.SaveChangesAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task ActualizarLista(IEnumerable<T> entidades, CancellationToken tokenDeCancelacion = default)
    {
        _contexto.Set<T>().UpdateRange(entidades);
        await _contexto.SaveChangesAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task Eliminar(T entidad, CancellationToken tokenDeCancelacion = default)
    {
        _contexto.Set<T>().Remove(entidad);
        await _contexto.SaveChangesAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task EliminarLista(IEnumerable<T> entidades, CancellationToken tokenDeCancelacion = default)
    {
        _contexto.Set<T>().RemoveRange(entidades);
        await _contexto.SaveChangesAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task EliminarLista(ISpecification<T> especificacion, CancellationToken tokenDeCancelacion = default)
    {
        var query = AplicarEspecificacion(especificacion);
        _contexto.Set<T>().RemoveRange(query);

        await SalvarCambios(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task<int> SalvarCambios(CancellationToken tokenDeCancelacion = default)
    {
        return await _contexto.SaveChangesAsync(tokenDeCancelacion);
    }

    /// <summary>
    /// Filtra las entidades de tipo <typeparamref name="T"/>, a aquellos que coinciden con la lógica de consulta encapsulada del
    /// <paramref name="especificacion"/>.
    /// </summary>
    /// <param name="especificacion">La lógica de consulta encapsulada.</param>
    /// <param name="evaluateCriteriaOnly">Si se debe evaluar solo los criterios de la especificación.</param>
    /// <returns>Las entidades filtradas como <see cref="IQueryable{T}"/>.</returns>
    protected virtual IQueryable<T> AplicarEspecificacion(ISpecification<T> especificacion, bool evaluateCriteriaOnly = false)
    {
        return _evaluadorDeEspecificaciones.GetQuery(_contexto.Set<T>().AsQueryable(), especificacion, evaluateCriteriaOnly);
    }

    /// <summary>
    /// Filtra las entidades de tipo <typeparamref name="T"/>, a aquellos que coinciden con la lógica de consulta encapsulada del
    /// <paramref name="especificacion"/>, de la base de datos.
    /// <para>
    /// Proyecta cada entidad en una nueva forma, siendo <typeparamref name="TResult" />.
    /// </para>
    /// </summary>
    /// <typeparam name="TResult">El tipo de valor devuelto por la proyección.</typeparam>
    /// <param name="especificacion">La lógica de consulta encapsulada.</param>
    /// <returns>Las entidades proyectadas filtradas como <see cref="IQueryable{T}"/>.</returns>
    protected virtual IQueryable<TResult> AplicarEspecificacion<TResult>(ISpecification<T, TResult> especificacion)
    {
        return _evaluadorDeEspecificaciones.GetQuery(_contexto.Set<T>().AsQueryable(), especificacion);
    }

    /// <inheritdoc/>
    public virtual async Task<T?> BuscarPorId<TId>(TId id, CancellationToken tokenDeCancelacion = default) where TId : notnull
    {
        return await _contexto.Set<T>().FindAsync(new object[] { id }, cancellationToken: tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task<T?> PrimeroOPredeterminado(ISpecification<T> especificacion, CancellationToken tokenDeCancelacion = default)
    {
        return await AplicarEspecificacion(especificacion).FirstOrDefaultAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task<TResultado?> PrimeroOPredeterminado<TResultado>(ISpecification<T, TResultado> especificacion, CancellationToken tokenDeCancelacion = default)
    {
        return await AplicarEspecificacion(especificacion).FirstOrDefaultAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task<T?> UnicoOPredeterminado(ISingleResultSpecification<T> especificacion, CancellationToken tokenDeCancelacion = default)
    {
        return await AplicarEspecificacion(especificacion).SingleOrDefaultAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task<TResultado?> UnicoOPredeterminado<TResultado>(ISingleResultSpecification<T, TResultado> especificacion, CancellationToken tokenDeCancelacion = default)
    {
        return await AplicarEspecificacion(especificacion).SingleOrDefaultAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task<bool> ContieneElemento(ISpecification<T> especificacion, CancellationToken tokenDeCancelacion = default)
    {
        return await AplicarEspecificacion(especificacion, true).AnyAsync(tokenDeCancelacion);
    }

    /// <inheritdoc/>
    public virtual async Task<bool> ContieneElemento(CancellationToken tokenDeCancelacion = default)
    {
        return await _contexto.Set<T>().AnyAsync(tokenDeCancelacion);
    }
}