﻿using Ardalis.Specification;
using DDD.Abstracciones.NucleoCompartido;

namespace Ddd.Abstracciones.NucleoCompartido.Repositorios;

/// <summary>
/// Funcionalidad de busqueda basica para un repositorio.
/// </summary>
/// <typeparam name="T"></typeparam>
public interface IRepositorioBusquedaBasica<T> where T : class, IRaizAgregada
{
    /// <summary>
    /// Hace una busqueda usando la llave primaria de <typeparamref name="T" />.
    /// </summary>
    /// <typeparam name="TId">El tipo de llave primaria.</typeparam>
    /// <param name="id">El valor de la llave primaria.</param>
    /// <param name="tokenDeCancelacion"></param>
    /// <returns>
    /// Una respuesta asincrona.
    /// La respuesta contiene un <typeparamref name="T" />, o <see langword="null"/>.
    /// </returns>
    Task<T?> BuscarPorId<TId>(TId id, CancellationToken tokenDeCancelacion = default) where TId : notnull;

    /// <summary>
    /// Devuelve el primer elemento de una secuencia o un valor predeterminado si la secuencia no contiene elementos.
    /// </summary>
    /// <param name="especificacion">La lógica de consulta encapsulada.</param>
    /// <param name="tokenDeCancelacion">Un <see cref="CancellationToken" /> a observar mientras se espera que se complete la tarea.</param>
    /// <returns>
    /// Una tarea que representa la operación asincrónica.
    /// El resultado de la tarea contiene el <typeparamref name="T" />, o <see langword="null"/>.
    /// </returns>
    Task<T?> PrimeroOPredeterminado(ISpecification<T> especificacion, CancellationToken tokenDeCancelacion = default);

    /// <summary>
    /// Devuelve el primer elemento de una secuencia o un valor predeterminado si la secuencia no contiene elementos.
    /// </summary>
    /// <param name="especificacion">La lógica de consulta encapsulada.</param>
    /// <param name="tokenDeCancelacion">Un <see cref="CancellationToken" /> a observar mientras se espera que se complete la tarea.</param>
    /// <returns>
    /// Una tarea que representa la operación asincrónica.
    /// El resultado de la tarea contiene el <typeparamref name="TResultado" />, o <see langword="null"/>.
    /// </returns>
    Task<TResultado?> PrimeroOPredeterminado<TResultado>(ISpecification<T, TResultado> especificacion, CancellationToken tokenDeCancelacion = default);

    /// <summary>
    /// Devuelve el único elemento de una secuencia, o un valor predeterminado si la secuencia está vacía; este método lanza una excepción si hay más de un elemento en la secuencia.
    /// </summary>
    /// <param name="especificacion">La lógica de consulta encapsulada.</param>
    /// <param name="tokenDeCancelacion">Un <see cref="CancellationToken" /> a observar mientras se espera que se complete la tarea.</param>
    /// <returns>
    /// Una tarea que representa la operación asincrónica.
    /// El resultado de la tarea contiene el <typeparamref name="T" />, or <see langword="null"/>.
    /// </returns>
    Task<T?> UnicoOPredeterminado(ISingleResultSpecification<T> especificacion, CancellationToken tokenDeCancelacion = default);

    /// <summary>
    /// Devuelve el único elemento de una secuencia, o un valor predeterminado si la secuencia está vacía; este método lanza una excepción si hay más de un elemento en la secuencia.
    /// </summary>
    /// <param name="especificacion">La lógica de consulta encapsulada.</param>
    /// <param name="tokenDeCancelacion">Un <see cref="CancellationToken" /> a observar mientras se espera que se complete la tarea.</param>
    /// <returns>
    /// Una tarea que representa la operación asincrónica.
    /// El resultado de la tarea contiene el <typeparamref name="TResultado" />, o <see langword="null"/>.
    /// </returns>
    Task<TResultado?> UnicoOPredeterminado<TResultado>(ISingleResultSpecification<T, TResultado> especificacion, CancellationToken tokenDeCancelacion = default);

    /// <summary>
    /// Devuelve un valor booleano que representa si alguna entidad satisface la lógica de consulta encapsulada.
    /// del tipo <paramref name="especificacion"/> or not.
    /// </summary>
    /// <param name="especificacion">La lógica de consulta encapsulada.</param>
    /// <param name="tokenDeCancelacion">Un <see cref="CancellationToken" /> a observar mientras se espera que se complete la tarea.</param>
    /// <returns>
    /// Una tarea que representa la operación asincrónica. El resultado de la tarea contiene verdadero si el 
    /// la secuencia fuente contiene cualquier elemento; en caso contrario, falso.
    /// </returns>
    Task<bool> ContieneElemento(ISpecification<T> especificacion, CancellationToken tokenDeCancelacion = default);

    /// <summary>
    ///Devuelve un valor booleano independientemente de que exista alguna entidad o no.
    /// </summary>
    /// <returns>
    /// Una tarea que representa la operación asincrónica. El resultado de la tarea contiene verdadero si el 
    /// la secuencia fuente contiene cualquier elemento; en caso contrario, falso.
    /// </returns>
    Task<bool> ContieneElemento(CancellationToken tokenDeCancelacion = default);
}