using Ardalis.Specification;
using Ddd.Abstracciones.NucleoCompartido.Repositorios;

namespace DDD.Abstracciones.NucleoCompartido.Repositorios;

/// <summary>
/// <para>
/// El <see cref="IRepositorioDeConsultas{T}" /> puede ser usado para implementaciones de busquedas a una base de datos para el agreagado del dominio de tipo <typeparamref name="T" />.
/// El <see cref="ISpecification{T}"/> (o derivados) es usado para encapsular la logica LINQ para hacer busquedas a la base de datos.
/// </para>
/// </summary>
/// <typeparam name="T">El tipo de agregado usado en este repositorio</typeparam>
public interface IRepositorioDeConsultas<T>: IRepositorioBusquedaBasica<T> where T : class, IRaizAgregada
{    
    /// <summary>
    /// Busca todas las entidades de tipo <typeparamref name="T" /> en la base de datos.
    /// </summary>
    /// <returns>
    /// Una tarea que representa la operación asincrónica.
    /// El resultado que contiene una lista del tipo dado <see cref="List{T}" /> .
    /// </returns>
    Task<List<T>> Listar(CancellationToken tokenDeCancelacion = default);

    /// <summary>
    /// Busca todas las entidades de tipo <typeparamref name="T" />, que coincide con la lógica de consulta encapsulada de la
    /// <paramref name="especificacion"/>, de la base de datos.
    /// </summary>
    /// <param name="especificacion">La lógica de consulta encapsulada.</param>
    /// <param name="tokenDeCancelacion">Un <see cref="CancellationToken" /> a observar mientras se espera que se complete la tarea.</param>
    /// <returns>
    /// Una tarea que representa la operación asincrónica.
    /// El resultado que contiene una lista del tipo dado <see cref="List{T}" /> .
    /// </returns>
    Task<List<T>> Listar(ISpecification<T> especificacion, CancellationToken tokenDeCancelacion = default);

    /// <summary>
    /// Busca todas las entidades de tipo <typeparamref name="T" />, que coincide con la lógica de consulta encapsulada de la
    /// <paramref name="especificacion"/>, de la base de datos.
    /// <para>
    /// Proyecta cada entidad en una nueva forma de tipo <typeparamref name="TResultado" />.
    /// </para>
    /// </summary>
    /// <typeparam name="TResultado">El tipo de valor devuelto por la proyección.</typeparam>
    /// <param name="especificacion">La lógica de consulta encapsulada.</param>
    /// <param name="tokenDeCancelacion">Un <see cref="CancellationToken" /> a observar mientras se espera que se complete la tarea.</param>
    /// <returns>
    /// Una tarea que representa la operación asincrónica.
    /// El resultado que contiene el tipo <see cref="List{TResultado}" /> la cual tiene una lista de elementos del tipo indicado.
    /// </returns>
    Task<List<TResultado>> Listar<TResultado>(ISpecification<T, TResultado> especificacion, CancellationToken tokenDeCancelacion = default);

    /// <summary>
    /// Devuelve un número que representa cuántas entidades satisfacen la lógica de consulta encapsulada.
    /// del tipo <paramref name="especificacion"/>.
    /// </summary>
    /// <param name="especificacion">La lógica de consulta encapsulada.</param>
    /// <param name="tokenDeCancelacion">Un <see cref="CancellationToken" /> a observar mientras se espera que se complete la tarea.</param>
    /// <returns>
    /// Una tarea que representa la operación asincrónica. El resultado de la tarea contiene el
    /// numero de elementos dentro de la sequencia indicada.
    /// </returns>
    Task<int> Contar(ISpecification<T> especificacion, CancellationToken tokenDeCancelacion = default);

    /// <summary>
    /// Devuelve el número total de registros..
    /// </summary>
    /// <returns>
    /// Una tarea que representa la operación asincrónica. El resultado de la tarea contiene el
    /// numero de elementos dentro de la sequencia indicada.
    /// </returns>
    Task<int> Contar(CancellationToken tokenDeCancelacion = default);
    
#if NET6_0_OR_GREATER
    /// <summary>
    /// Busca todas las entidades de tipo <typeparamref name="T" />, que coincide con la lógica de consulta encapsulada del
    /// <paramref name="especificacion"/>, de la base de datos.
    /// </summary>
    /// <param name="especificacion">La lógica de consulta encapsulada.</param>
    /// <returns>
    ///  Regresa un IAsyncEnumerable el cual puede ser enumerado de forma asincrona.
    /// </returns>
    IAsyncEnumerable<T> ComoEnumerable(ISpecification<T> especificacion);
#endif
}