using Ardalis.Specification;
using Ddd.Abstracciones.NucleoCompartido.Repositorios;

namespace DDD.Abstracciones.NucleoCompartido.Repositorios;
/// <summary>
/// El <see cref="IRepositorioDeComandos{T}" /> puede ser usado para salvar instancias de objeto de un agregado tipo <typeparamref name="T"/> a una base de datos.
/// Un tipo de <see cref="ISpecification{T}"/> (o su derivado) es usado para encapsular el LINQ a usar para la base de datos.
/// </summary>
/// <typeparam name="T">El tipo de agregado a salvar en la base de datos.</typeparam>
public interface IRepositorioDeComandos<T>: IRepositorioBusquedaBasica<T> where T : class, IRaizAgregada
{
    /// <summary>
    /// Agrega una entidad a la base de datos.
    /// </summary>
    /// <param name="entidad">La entidad a agregar.</param>
    /// <param name="tokenDeCancelacion">Token de Cancelacion</param>
    /// <returns>
    /// Una tarea que representa la operación asincrónica..
    /// El resultado de la tarea contiene el <typeparamref name="T" />.
    /// </returns>
    Task<T> Agregar(T entidad, CancellationToken tokenDeCancelacion = default);

    /// <summary>
    /// Agrega las entidades dadas en la base de datos.
    /// </summary>
    /// <param name="entidades">La lista de entidades</param>
    /// <param name="tokenDeCancelacion">El token de cancelacion</param>
    /// <returns>
    /// Una tarea que representa la operación asincrónica..
    /// </returns>
    Task<IEnumerable<T>> AgregarLista(IEnumerable<T> entidades, CancellationToken tokenDeCancelacion = default);

    /// <summary>
    /// Actualiza una entidad en la base de datos
    /// </summary>
    /// <param name="entidad">La entidad a actualizar.</param>
    /// <param name="tokenDeCancelacion"></param>
    /// <returns>Una tarea que representa la operación asincrónica..</returns>
    Task Actualizar(T entidad, CancellationToken tokenDeCancelacion = default);

    /// <summary>
    /// Actualiza las entidades dadas en la base de datos.
    /// </summary>
    /// <param name="entidades">La lista de entidades a actualizar.</param>
    /// <param name="tokenDeCancelacion"></param>
    /// <returns>Una tarea que representa la operación asincrónica..</returns>
    Task ActualizarLista(IEnumerable<T> entidades, CancellationToken tokenDeCancelacion = default);

    /// <summary>
    /// Elimina la entidad dada en la base de datos.
    /// </summary>
    /// <param name="entidad">La entidad a eliminar.</param>
    /// <param name="tokenDeCancelacion"></param>
    /// <returns>Una tarea que representa la operación asincrónica..</returns>
    Task Eliminar(T entidad, CancellationToken tokenDeCancelacion = default);

    /// <summary>
    /// Elimina las entidades dadas en la base de datos.
    /// </summary>
    /// <param name="entidades">Las lista de entidades a eliminar.</param>
    /// <param name="tokenDeCancelacion"></param>
    /// <returns>Una tarea que representa la operación asincrónica..</returns>
    Task EliminarLista(IEnumerable<T> entidades, CancellationToken tokenDeCancelacion = default);

    /// <summary>
    /// Elimina todas las entidades de tipo <typeparamref name="T" />, que coincide con la lógica de consulta encapsulada del
    /// <paramref name="especificacion"/>, de la base de datos.
    /// </summary>
    /// <param name="especificacion">La lógica de consulta encapsulada.</param>
    /// <param name="tokenDeCancelacion"></param>
    /// <returns>Una tarea que representa la operación asincrónica..</returns>
    Task EliminarLista(ISpecification<T> especificacion, CancellationToken tokenDeCancelacion = default);

    /// <summary>
    /// Persiste los cambios en la base de datos.
    /// </summary>
    /// <returns>Una tarea que representa la operación asincrónica.</returns>
    Task<int> SalvarCambios(CancellationToken tokenDeCancelacion = default);    
}