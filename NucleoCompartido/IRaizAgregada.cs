namespace DDD.Abstracciones.NucleoCompartido;

/// <summary>
/// Indica que la clase es una raíz agregada.
/// </summary>
public interface IRaizAgregada { }