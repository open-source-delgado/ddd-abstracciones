namespace DDD.Abstracciones.NucleoCompartido.APIs;
/// <summary>
/// Base para las llamadas y respuestas a APIs.
/// </summary>
public abstract class MensajeBase
{

    /// <summary>
    /// Id de correlación.
    /// </summary>
    /// <returns></returns>
    protected Guid _idDeCorrelacion = Guid.NewGuid();

    /// <summary>
    /// Id de correlación.
    /// </summary>
    /// <returns></returns>
    public Guid IdDeCorrelacion() => _idDeCorrelacion;
}
