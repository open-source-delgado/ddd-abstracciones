namespace DDD.Abstracciones.NucleoCompartido.APIs;
/// <summary>
/// Base para las respuestas a APIs.
/// </summary>
public abstract class RespuestaBase : MensajeBase
{
    /// <summary>
    /// Constructor de <see cref="RespuestaBase"/>.
    /// </summary>
    /// <param name="idDeCorrelacion"></param>    
    public RespuestaBase(Guid idDeCorrelacion) : base()
    {
        base._idDeCorrelacion = idDeCorrelacion;
    }

    /// <summary>
    /// Constructor de <see cref="RespuestaBase"/>.
    /// </summary>
    public RespuestaBase()
    {

    }
}