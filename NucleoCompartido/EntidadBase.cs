using DDD.Abstracciones.NucleoCompartido.Eventos;
namespace DDD.Abstracciones.NucleoCompartido;
/// <summary>
/// La entidad base usando el Id otorgado con tipo generico
/// </summary>
public abstract class EntidadBase<TId>
{
    /// <summary>
    /// El Id de la entidad de tipo generico
    /// </summary>
    /// <value></value>
    public TId Id { get; set; }

    /// <summary>
    /// Lista de eventos del dominio y de integracion
    /// </summary>
    public List<EventoBase> Eventos = new();
}
