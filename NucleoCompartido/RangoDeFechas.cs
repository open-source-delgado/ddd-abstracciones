using Ardalis.GuardClauses;
namespace DDD.Abstracciones.NucleoCompartido;
/// <summary>
/// Objeto de valor que representa un rango de fechas.
/// </summary>
public class RangoDeFecha : ObjetoDeValor
{
    /// <summary>
    /// El comienzo del rango de fechas.
    /// </summary>
    /// <value></value>
    public DateTimeOffset Comienzo { get; private set; }

    /// <summary>
    /// El fin del rango de fechas.
    /// </summary>
    /// <value></value>
    public DateTimeOffset Fin { get; private set; }

    /// <summary>
    /// Inicia una nueva instancia de <see cref="RangoDeFecha" />.
    /// </summary>
    /// <param name="comienzo">El comienzo del rango de fechas.</param>
    /// <param name="fin">El fin del rango de fechas</param>
    /// <exception cref="ArgumentOutOfRangeException">Si <paramref name="comienzo" /> es mayor que <paramref name="fin" />.</exception>
    public RangoDeFecha(DateTimeOffset comienzo, DateTimeOffset fin)
    {
        Guard.Against.OutOfRange(comienzo, nameof(comienzo), comienzo, fin);
        Comienzo = comienzo;
        Fin = fin;
    }

    /// <summary>
    /// Inicia una nueva instancia de <see cref="RangoDeFecha" />.
    /// </summary>
    /// <param name="comienzo">El comienzo de un rango de fechas.</param>
    /// <param name="duracion">La duracion de un rango de fechas.</param>    
    public RangoDeFecha(DateTimeOffset comienzo, TimeSpan duracion) : this(comienzo, comienzo.Add(duracion))
    {
    }

    /// <summary>
    /// La duracion del rango de fechas en minutos.
    /// </summary>
    /// <returns>El numero entero indicando la duracion del rango de fechas.</returns>
    public int DuracionEnMinutos()
    {
        return (int)Math.Round((Fin - Comienzo).TotalMinutes, 0);
    }

    /// <summary>
    /// Crea un nuevo rango de fechas con la duracion especificada.
    /// </summary>
    /// <param name="nuevaDuracion">La nueva duracion del rango de fecha.</param>
    /// <returns>El nuevo rango de fechas de <see cref="RangoDeFecha"/> </returns>
    public RangoDeFecha NuevaDuracion(TimeSpan nuevaDuracion)
    {
        return new RangoDeFecha(this.Comienzo, nuevaDuracion);
    }

    /// <summary>
    /// Crea un nuevo rango de fechas con el fin especificado.
    /// </summary>
    /// <param name="nuevoFin">El nuevo fin de rango de fechas.</param>
    /// <returns>El nuevo rango de fechas de <see cref="RangoDeFecha"/></returns>
    public RangoDeFecha NuevoFin(DateTimeOffset nuevoFin)
    {
        return new RangoDeFecha(this.Comienzo, nuevoFin);
    }

    /// <summary>
    /// Crea un nuevo rango de fechas con el comienzo especificado.
    /// </summary>
    /// <param name="nuevoComienzo">El nuevo comienzo del rango de fechas.</param>
    /// <returns>El nuevo rango de fechas de <see cref="RangoDeFecha"/></returns>
    public RangoDeFecha NuevoComienzo(DateTimeOffset nuevoComienzo)
    {
        return new RangoDeFecha(nuevoComienzo, this.Fin);
    }

    /// <summary>
    /// Crea un nuevo rango de fechas de un dia comenzando con la fecha indicada.
    /// </summary>
    /// <param name="dia">El dia incluido en el rango de fecha.</param>
    /// <returns>El rango de fecha creado que dura un dia.</returns>
    public static RangoDeFecha CrearRangoDeUnDia(DateTimeOffset dia)
    {
        return new RangoDeFecha(dia, dia.AddDays(1));
    }

    /// <summary>
    /// Crea un rango de fecha de una semana comenzando con la fecha indicada.
    /// </summary>
    /// <param name="diaDeComienzo">Dia de comienzo del rango de fechas que dura una semana.</param>
    /// <returns>El rango de fechas <see cref="RangoDeFecha"/></returns>
    public static RangoDeFecha CrearUnRangoDeUnaSemana(DateTimeOffset diaDeComienzo)
    {
        return new RangoDeFecha(diaDeComienzo, diaDeComienzo.AddDays(7));
    }

    /// <summary>
    /// Indica si existe traslapo en el rango de fechas indicado.
    /// </summary>
    /// <param name="rangoDeFecha">Rango de fechas a verificar de tipo <see cref="RangoDeFecha"/></param>
    /// <returns></returns>
    public bool ExisteTraslapo(RangoDeFecha rangoDeFecha)
    {
        return this.Comienzo < rangoDeFecha.Fin &&
            this.Fin > rangoDeFecha.Comienzo;
    }

    /// <summary>
    /// Lista de elementos dentro del rangos de fecha 
    /// </summary>
    /// <returns>Lista de elementos dentro del rango de fecha</returns>
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Comienzo;
        yield return Fin;
    }

}
