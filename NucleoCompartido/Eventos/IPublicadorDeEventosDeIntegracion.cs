namespace DDD.Abstracciones.NucleoCompartido.Eventos;
/// <summary>
/// Interfaz para publicar eventos de integración.
/// </summary>
public interface IPublicadorDeEventosDeIntegracion
{
    /// <summary>
    /// Envia un evento de integración a un queue.
    /// </summary>
    /// <param name="evento">Evento</param>
    /// <param name="nombreDelQueue">Nombre del Queue</param>    
    Task PublicarEventoAlQueue(EventoBase evento, string nombreDelQueue);

    /// <summary>
    /// Envia un evento de integración a un topic (tambien conocido como tema o intercambio).
    /// </summary>
    /// <param name="evento">Evento</param>
    /// <param name="nombreDelTopic">Nombre del Topic (tambien conocido como tema o intercambio)</param>
    Task PublicarEventoAlTopic(EventoBase evento, string nombreDelTopic);
}