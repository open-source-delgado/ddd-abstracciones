namespace DDD.Abstracciones.NucleoCompartido.Eventos;
/// <summary>
/// Base para los eventos de integración.
/// </summary>
public abstract class EventoDeIntegracionBase : EventoBase
{
    /// <summary>
    /// Constructor de <see cref="EventoDeIntegracionBase"/>.
    /// </summary>
    public EventoDeIntegracionBase()
    {
        EsEventoDeIntegracion = true;
    }
}