namespace DDD.Abstracciones.NucleoCompartido.Eventos
{
    /// <summary>
    /// Base para eventos de dominio.
    /// </summary>
    public abstract class EventoDeDominioBase : EventoBase
    {
        /// <summary>
        /// Instancia de <see cref="EventoDeDominioBase"/>
        /// </summary>
        public EventoDeDominioBase()
        {
            EsEventoDeIntegracion = false;
        }
    }
}