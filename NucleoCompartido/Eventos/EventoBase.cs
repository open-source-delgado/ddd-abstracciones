using MediatR;
namespace DDD.Abstracciones.NucleoCompartido.Eventos;
/// <summary>
/// Base para los eventos.
/// </summary>
public abstract class EventoBase : INotification
{
    /// <summary>
    /// Indica si el evento es de integracion o de dominio.
    /// <para>True: Es evento de integracion, se publica en el bus de integracion</para>
    /// <para>False: Es evento de dominio, se publica internamente dentro del dominio</para>
    /// <para>Valor predeterminado = false</para>
    /// </summary>
    public bool EsEventoDeIntegracion { get; protected set; } = false;

    /// <summary>
    /// Fecha UTC en la que se ha creado el evento
    /// <para>Valor predeterminado = DateTimeOffset.UtcNow</para>
    /// </summary>
    public DateTimeOffset FechaUtcDelEvento { get; protected set; } = DateTimeOffset.UtcNow;

    /// <summary>
    /// Nombre del evento
    /// </summary>
    public string NombreDelEvento { get; protected set; } = string.Empty;

    /// <summary>
    /// El Id del evento.
    /// <para>Valor predeterminado = Guid.NewGuid()</para>
    /// </summary>
    public Guid Id { get; protected set; } = Guid.NewGuid();
}
